$(document).ready(function(){
    $('.slider').slick({
    infinite: true,
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	      }
	    }
    ]
    });
});


    